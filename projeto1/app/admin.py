from django.contrib import admin

from app.models import Actor, Movie, Director, Genre, \
    Country, Language, Keyword

# Register your models here.
admin.site.register(Actor)
admin.site.register(Movie)
admin.site.register(Director)
admin.site.register(Genre)
admin.site.register(Country)
admin.site.register(Language)
admin.site.register(Keyword)
