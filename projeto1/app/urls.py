from django.urls import path
import app.views as app_views

app_name = "app"

urlpatterns = [
    path("", app_views.home, name="home"),
    # Actors
    path("actor/", app_views.actor_endpoint, name="actor"),
    path("actor/<int:actor_id>", app_views.actor_endpoint, name="actor"),
    path("actor/search/", app_views.search_actor, name="actor_search"),
    # Directors
    path("director/", app_views.director_endpoint, name="director"),
    path("director/<int:director_id>", app_views.director_endpoint, name="director"),
    path("director/search/", app_views.search_director, name="director_search"),
    # Movies
    path("movie/", app_views.movie_endpoint, name="movie"),
    path("movie/<int:movie_id>", app_views.movie_endpoint, name="movie"),
    path("movie/search/", app_views.search_movie, name="movie_search"),
    # Languages
    path("language/", app_views.language_endpoint, name="language"),
    path("language/<int:language_id>", app_views.language_endpoint, name="language"),
    path("language/search/", app_views.search_language, name="language_search"),
    # Countries
    path("country/", app_views.country_endpoint, name="country"),
    path("country/<int:country_id>", app_views.country_endpoint, name="country"),
    path("country/search/", app_views.search_country, name="country_search"),
    # Genres
    path("genre/", app_views.genre_endpoint, name="genre"),
    path("genre/<int:genre_id>", app_views.genre_endpoint, name="genre"),
    path("genre/search/", app_views.search_genre, name="genre_search"),
    # Keywords
    path("keyword/", app_views.keyword_endpoint, name="keyword"),
    path("keyword/<int:keyword_id>", app_views.keyword_endpoint, name="keyword"),
    path("keyword/search/", app_views.search_keyword, name="keyword_search"),
    path("init_db/<int:confirmation>", app_views.start_script, name="exemplo")
]
