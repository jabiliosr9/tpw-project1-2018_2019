from django.db.models import Model
from typing import Type, Optional


def get_object_by_pk(model: Type[Model], pk) -> Optional[Type[Model]]:
    try:
        obj = model.objects.get(pk=pk)
        return obj
    except model.DoesNotExist:
        return None
