'''
Movie
  #color,
  #num_critic_for_reviews,
  #duration,
  #gross,
  #genres,
  #movie_title,
  #num_voted_users,
  #cast_total_facebook_likes,
  #facenumber_in_poster,
  #plot_keywords,
  #movie_imdb_link,
  #num_user_for_reviews,
  #language,
  #country,
  #content_rating,
  #budget,
  #title_year,
  #imdb_score,
  #aspect_ratio,
  #movie_facebook_likes

Director 
  director_name,
  director_facebook_likes,


Actor
  actor_3_facebook_likes,
  actor_2_name,
  actor_1_facebook_likes,
  actor_1_name,
  actor_3_name,
  actor_2_facebook_likes,


'''

from django.db import models


# Create your models here.


class Director(models.Model):
    name = models.CharField(max_length=128, unique=True)
    facebook_likes = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"            : self.pk,
            "name"          : self.name,
            "facebook_likes": self.facebook_likes,
        }


class Language(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"  : self.pk,
            "name": self.name,
        }


class Country(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"  : self.pk,
            "name": self.name,
        }


class Actor(models.Model):
    name = models.CharField(max_length=128, unique=True)
    facebook_likes = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"            : self.pk,
            "name"          : self.name,
            "facebook_likes": self.facebook_likes,
        }


class Genre(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"  : self.pk,
            "name": self.name,
        }


class Keyword(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return f"{self.pk} - {self.name}"

    def to_json(self):
        return {
            "id"  : self.pk,
            "name": self.name,
        }


class Movie(models.Model):
    class Color:
        COLOR = "COLOR"
        BLACK_AND_WHITE = "Black and White"
        MOVIE_COLOR = (
            (COLOR, "Color"),
            (BLACK_AND_WHITE, "Black and White")
        )

    class Rating:
        APPROVED = "Approved"
        G = "G"
        GP = "GP"
        M = "M"
        NC_17 = "NC_17"
        Passed = "Ppassed"
        PG = "PG"
        PG_13 = "PG_13"
        R = "R"
        TV_14 = "TV_14"
        TV_G = "TV_G"
        TV_MA = "TV_MA"
        TV_PG = "TV_PG"
        TV_Y = "TV_Y"
        TV_Y7 = "TV_y7"
        Unrated = "Unrated"
        X = "X"
        MOVIE_RATING = (
            (APPROVED, "Approved"),
            (G, "G"),
            (GP, "GP"),
            (M, "M"),
            (NC_17, "NC-17"),
            (Passed, "Passed"),
            (PG, "PG"),
            (PG_13, "PG-13"),
            (R, "R"),
            (TV_14, "TV-14"),
            (TV_G, "TV-G"),
            (TV_MA, "TV-MA"),
            (TV_PG, "TV-PG"),
            (TV_Y, "TV-Y"),
            (TV_Y7, "TV-Y7"),
            (Unrated, "Unrated"),
            (X, "X"),
        )

    color = models.CharField(max_length=30, choices=Color.MOVIE_COLOR, null=True)
    director = models.ForeignKey(Director, on_delete=models.CASCADE, null=True)
    num_critic_for_reviews = models.IntegerField(null=True)
    duration = models.DurationField(null=True)
    gross = models.BigIntegerField(null=True)
    movie_title = models.CharField(max_length=128, unique=True)
    num_voted_users = models.IntegerField(default=0)
    cast_total_facebook_likes = models.IntegerField(default=0)
    face_number_in_poster = models.IntegerField(null=True)
    movie_imdb_link = models.URLField()
    num_user_for_reviews = models.IntegerField(null=True)
    language = models.ForeignKey(Language, related_name="country", on_delete=models.CASCADE,
                                 null=True, blank=True)
    content_rating = models.CharField(max_length=30, choices=Rating.MOVIE_RATING, null=True)
    budget = models.BigIntegerField(null=True)
    title_year = models.IntegerField(null=True)
    imdb_score = models.DecimalField(max_digits=4, decimal_places=1, null=True)
    aspect_ratio = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    movie_facebook_likes = models.IntegerField(default=0)
    country = models.ForeignKey(Country, related_name="country", on_delete=models.CASCADE,
                                null=True, blank=True)
    actor_1 = models.ForeignKey(Actor, related_name="actor_1", on_delete=models.CASCADE, null=True,
                                blank=True)
    actor_2 = models.ForeignKey(Actor, related_name="actor_2", on_delete=models.CASCADE, null=True,
                                blank=True)
    actor_3 = models.ForeignKey(Actor, related_name="actor_3", on_delete=models.CASCADE, null=True,
                                blank=True)
    genres = models.ManyToManyField(Genre)
    keywords = models.ManyToManyField(Keyword)

    def __str__(self):
        return f"{self.pk} - {self.title_year} - {self.movie_title}"

    def to_simple_json(self):
        return {
            "id"            : self.pk,
            "title"         : self.movie_title,
            "year"          : self.title_year,
            "facebook_likes": self.movie_facebook_likes,
            "imdb"          : self.movie_imdb_link,
            "score"         : self.imdb_score,
        }
