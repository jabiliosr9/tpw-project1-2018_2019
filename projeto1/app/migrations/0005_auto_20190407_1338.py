# Generated by Django 2.1.7 on 2019-04-07 12:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20190407_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='actor_1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='actor_1', to='app.Actor'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='actor_2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='actor_2', to='app.Actor'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='actor_3',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='actor_3', to='app.Actor'),
        ),
    ]
