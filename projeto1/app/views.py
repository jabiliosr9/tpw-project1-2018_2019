from http.client import HTTPResponse
from typing import Optional, Type, List

# Create your views here.
from django.db import models
from django.http import JsonResponse
from django.shortcuts import render
import django.utils.html

from app.models import Actor, Movie
from app.models import Director, Genre, Country, Language, Keyword
from app.utils import get_object_by_pk
import random
from django.db.models import Count


def _stats():
    stats = [
        {
            "label": "Atores",
            "value": Actor.objects.count()
        },
        {
            "label": "Diretores",
            "value": Director.objects.count()
        },
        {
            "label": "Filmes",
            "value": Movie.objects.count()
        },
        {
            "label": "Países",
            "value": Country.objects.count()
        },
        {
            "label": "Géneros",
            "value": Genre.objects.count()
        },
        {
            "label": "Linguas",
            "value": Language.objects.count()
        },
        {
            "label": "Palavras-Chave",
            "value": Keyword.objects.count()
        },
    ]
    return stats


def search_entity_by_name(cls: Type[models.Model], name: str) -> List[models.Model]:
    res = cls.objects.filter(name__startswith=name).all()
    return res


def random_entity(cls: Type[models.Model]) -> Type[models.Model]:
    return cls.objects.get(pk=random.randint(0, cls.objects.count()))


#################################
def start_script(request, confirmation):
    if confirmation != 99:
        print("Warning: Confirmation is not 99, not running script")
        return
    file = open("movie_metadata_v2.csv", "r", encoding="utf8")
    headers = file.readline()
    for row in file:
        values = [i.strip() if i else None for i in row.split(";")]
        # Movie related attributes
        movie_title = values[11]
        movie = Movie.objects.get_or_create(movie_title=movie_title)[0]
        color = values[0]
        if color is not None:
            movie.color = color
        num_critic_for_reviews = values[2]
        duration = values[3]
        gross = values[8]
        if gross is not None:
            movie.gross = gross
        genres = values[9]
        if genres is not None:
            genres = [i.strip().lower() for i in genres.split("|")]
            for genre in genres:
                g = Genre.objects.get_or_create(name=genre)[0]
                g.movie_set.add(movie)
        num_voted_users = values[12]
        cast_total_facebook_likes = values[13]
        facenumber_in_poster = values[15]
        plot_keywords = values[16]

        if plot_keywords is not None:
            plot_keywords = [i.strip().lower() for i in plot_keywords.split("|")]
            for keyword in plot_keywords:
                k = Keyword.objects.get_or_create(name=keyword)[0]
                k.movie_set.add(movie)
        movie_imdb_link = values[17]
        if movie_imdb_link is not None:
            movie.movie_imdb_link = movie_imdb_link
        num_user_for_reviews = values[18]

        language = values[19]
        if language is not None:
            language = Language.objects.get_or_create(name=language.lower().capitalize())[0]
            movie.language = language
        country = values[20]
        if country is not None:
            country = Country.objects.get_or_create(name=country.lower().capitalize())[0]
            movie.country = country
        content_rating = values[21]
        if content_rating is not None:
            movie.content_rating = content_rating
        budget = values[22]
        if budget is not None:
            movie.budget = budget
        title_year = values[23]
        if title_year is not None:
            movie.title_year = title_year
        imdb_score = values[25]
        if imdb_score is not None:
            movie.imdb_score = imdb_score
        movie_facebook_likes = values[27]
        if movie_facebook_likes is not None:
            movie.movie_facebook_likes = movie_facebook_likes
        aspect_ratio = values[26]

        # after creating the movie, set the actors and director if they exist

        # Director related attributes
        director_name = values[1]
        director_facebook_likes = values[4]
        if director_name is not None:
            director = Director.objects.get_or_create(name=director_name)[0]
            director.facebook_likes = director_facebook_likes
            # save the director
            director.save()
            # make the relation
            movie.director = director
        # Actor 1
        actor_1_name = values[10]
        actor_1_facebook_likes = values[7]
        if actor_1_name is not None:
            actor_1 = Actor.objects.get_or_create(name=actor_1_name)[0]
            actor_1.facebook_likes = actor_1_facebook_likes
            actor_1.save()
            movie.actor_1 = actor_1

        # Actor 2
        actor_2_facebook_likes = values[24]
        actor_2_name = values[6]
        if actor_2_name is not None:
            actor_2 = Actor.objects.get_or_create(name=actor_2_name)[0]
            actor_2.facebook_likes = actor_2_facebook_likes
            actor_2.save()
            movie.actor_2 = actor_2
        # Actor 3
        actor_3_name = values[14]
        actor_3_facebook_likes = values[5]
        if actor_3_name is not None:
            actor_3 = Actor.objects.get_or_create(name=actor_3_name)[0]
            actor_3.facebook_likes = actor_3_facebook_likes
            actor_3.save()
            movie.actor_3 = actor_3
        # finally, save the movie
        movie.save()
        print(f"Finished Movie {movie}")
    return HTTPResponse("Finished")


#################
# Utility Functions
#################

def get_unique_stats_for_movies(movies: List[Movie]):
    movie_stats = {
        "languages": set(),
        "genres"   : set(),
        "keywords" : set(),
        "countries": set(),
        "directors": set(),
    }
    for movie in movies:
        movie_stats["languages"].add(movie.language)
        for genre in movie.genres.all():
            movie_stats["genres"].add(genre)
        for keyword in movie.keywords.all():
            movie_stats["keywords"].add(keyword)
        movie_stats["countries"].add(movie.country)
        movie_stats["directors"].add(movie.director)
    return movie_stats


def get_previous_next_entities_by_pk(cls: Type[models.Model], entity: models.Model):
    ctx = {

    }
    try:
        ctx["previous"] = cls.objects.get(pk=entity.pk - 1)
    except cls.DoesNotExist:
        pass
    try:
        ctx["next"] = cls.objects.get(pk=entity.pk + 1)
    except cls.DoesNotExist:
        pass
    return ctx


#################
# Search
#################

def search_actor(request):
    actor_name = request.GET.get("name", None)
    if actor_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    actor_name = django.utils.html.escape(actor_name)
    order = request.GET.get('order', "ALF")
    if order == "LIK":
        actors = Actor.objects.filter(name__startswith=actor_name).order_by(
            "-facebook_likes")
    elif order == "ALF":
        actors = Actor.objects.filter(name__startswith=actor_name).order_by("name")
    else:
        actors = Actor.objects.filter(name__startswith=actor_name).order_by("name")
    return JsonResponse([i.to_json() for i in actors], safe=False)


def search_movie(request):
    movie_name = request.GET.get("name", None)
    if movie_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    movie_name = django.utils.html.escape(movie_name)
    order = request.GET.get('order', "ALF")
    if order == "YEAR":
        movies = Movie.objects.filter(movie_title__startswith=movie_name).order_by(
            "-title_year")
    elif order == "ALF":
        movies = Movie.objects.filter(movie_title__startswith=movie_name).order_by("movie_title")
    else:
        movies = Movie.objects.filter(movie_title__startswith=movie_name).order_by("movie_title")
    return JsonResponse([i.to_simple_json() for i in movies], safe=False)


def search_director(request):
    director_name = request.GET.get("name", None)
    if director_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    director_name = django.utils.html.escape(director_name)
    order = request.GET.get('order', "ALF")
    if order == "LIK":
        directors = Director.objects.filter(name__startswith=director_name).order_by(
            "-facebook_likes")
    elif order == "ALF":
        directors = Director.objects.filter(name__startswith=director_name).order_by("name")
    else:
        directors = Director.objects.filter(name__startswith=director_name).order_by("name")
    return JsonResponse([i.to_json() for i in directors], safe=False)


def search_language(request):
    language_name = request.GET.get("name", None)
    if language_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    language_name = django.utils.html.escape(language_name)
    order = request.GET.get('order', "ALF")
    if order == "ALF":
        languages = Language.objects.filter(name__startswith=language_name).order_by("name")
    else:
        languages = Language.objects.filter(name__startswith=language_name).order_by("name")
    return JsonResponse([i.to_json() for i in languages], safe=False)


def search_keyword(request):
    keyword_name = request.GET.get("name", None)
    if keyword_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    keyword_name = django.utils.html.escape(keyword_name)
    order = request.GET.get('order', "ALF")
    if order == "ALF":
        keywords = Keyword.objects.filter(name__startswith=keyword_name).order_by("name")
    elif order == "MOV":
        keywords = Keyword.objects.filter(name__startswith=keyword_name).annotate(
            num_movies=Count("movie")).order_by("-num_movies")
    else:
        keywords = Keyword.objects.filter(name__startswith=keyword_name).order_by("name")
    return JsonResponse([i.to_json() for i in keywords], safe=False)


def search_genre(request):
    genre_name = request.GET.get("name", None)
    if genre_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    genre_name = django.utils.html.escape(genre_name)
    order = request.GET.get('order', "ALF")
    genres = Genre.objects.filter(name__startswith=genre_name).annotate(
        num_movies=Count("movie"))
    if order == "ALF":
        genres = genres.order_by("name")
    elif order == "MOV":
        genres = genres.order_by("-num_movies")
    else:
        genres = genres.order_by("name")
    return JsonResponse([i.to_json() for i in genres], safe=False)


def search_country(request):
    country_name = request.GET.get("name", None)
    if country_name is None:
        return JsonResponse({
            "msg": "You need to provide a name!"
        })
    country_name = django.utils.html.escape(country_name)
    # TODO: order por filmes
    order = request.GET.get('order', "ALF")
    if order == "ALF":
        countries = Country.objects.filter(name__startswith=country_name).order_by("name")
    else:
        countries = Country.objects.filter(name__startswith=country_name).order_by("name")
    return JsonResponse([i.to_json() for i in countries], safe=False)


#################
# Endpoints
#################
def home(request):
    ctx = {
        "stats"       : _stats(),
        "random_movie": random_entity(Movie),
        "random_actor": random_entity(Actor),
    }
    return render(request, "index.html", ctx)


def director_endpoint(request, director_id):
    director = Director.objects.get(pk=director_id)
    movies = director.movie_set.all()
    ctx = {
        "director": director,
        "unique_stats": get_unique_stats_for_movies(movies),
        "movies": [i.to_simple_json() for i in movies]
    }
    return render(request, 'director.html', ctx)


def movie_endpoint(request, movie_id: int):
    movie = Movie.objects.get(pk=movie_id)
    ctx = {
        "movie"       : movie,
        "unique_stats": get_unique_stats_for_movies([movie])
    }
    actors = []
    if movie.actor_1:
        actors += [movie.actor_1]
    if movie.actor_2:
        actors += [movie.actor_2]
    if movie.actor_3:
        actors += [movie.actor_3]
    ctx["actors"] = actors
    return render(request, "movie_details.html", ctx)


def language_endpoint(request, language_id: int):
    language = Language.objects.get(pk=language_id)
    movies = Movie.objects.filter(language=language)
    ctx = {
        "language": language,
        "movies"  : movies,
    }
    # Get next and previous
    ctx.update(get_previous_next_entities_by_pk(Language, language))
    return render(request, 'language.html', ctx)


def keyword_endpoint(request, keyword_id):
    keyword = Keyword.objects.get(pk=keyword_id)
    ctx = {
        "keyword": keyword,
        "movies" : keyword.movie_set.all(),
    }
    # Get next and previous
    ctx.update(get_previous_next_entities_by_pk(Keyword, keyword))
    return render(request, 'keyword.html', ctx)


def actor_endpoint(request, actor_id: int = -1):
    actor: Optional[Actor] = get_object_by_pk(Actor, actor_id)
    if actor is None:
        return JsonResponse({
            "msg": "Actor not found!"
        })
    ctx = {
        "actor": actor
    }
    movies_jsons = []
    movies_obj = []
    for movie in Movie.objects.filter(actor_1=actor):
        movie_json = movie.to_simple_json()
        movie_json["actorOrder"] = 1
        movies_jsons += [movie_json]
        movies_obj += [movie]
    for movie in Movie.objects.filter(actor_2=actor):
        movie_json = movie.to_simple_json()
        movie_json["actorOrder"] = 2
        movies_jsons += [movie_json]
        movies_obj += [movie]
    for movie in Movie.objects.filter(actor_3=actor):
        movie_json = movie.to_simple_json()
        movie_json["actorOrder"] = 3
        movies_jsons += [movie_json]
        movies_obj += [movie]
    ctx["movies"] = movies_jsons
    ctx["unique_stats"] = get_unique_stats_for_movies(movies_obj)
    return render(request, 'ator_details.html', ctx)


def genre_endpoint(request, genre_id: int):
    genre = Genre.objects.get(pk=genre_id)
    ctx = {
        "genre" : genre,
        "movies": genre.movie_set.all(),
    }
    # Get next and previous
    ctx.update(get_previous_next_entities_by_pk(Genre, genre))
    return render(request, 'genre.html', ctx)


def country_endpoint(request, country_id: int):
    country = Country.objects.get(pk=country_id)
    movies = Movie.objects.filter(country=country)
    ctx = {
        "country": country,
        "movies" : movies,
    }
    # Get next and previous
    ctx.update(get_previous_next_entities_by_pk(Country, country))
    return render(request, 'country.html', ctx)
