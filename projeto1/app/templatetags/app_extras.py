from django import template

from app.models import Actor, Director, Movie
from django.contrib.staticfiles.templatetags.staticfiles import static

register = template.Library()


@register.inclusion_tag('movie_table_row.html')
def movies_to_table_row(movie):
    return movie


@register.inclusion_tag('unique_worked_with.html')
def unique_stats(label, url, objs):
    return label, url, objs


@register.inclusion_tag('actor_pagination.html')
def actor_pagination(actor: Actor):
    ctx = {

    }
    try:
        ctx["previous"] = Actor.objects.get(pk=actor.pk - 1)
    except Actor.DoesNotExist:
        pass
    try:
        ctx["next"] = Actor.objects.get(pk=actor.pk + 1)
    except Actor.DoesNotExist:
        pass
    return ctx


########################################################################
@register.inclusion_tag('director_pagination.html')
def director_pagination(director: Director):
    ctx = {

    }
    try:
        ctx["previous"] = Director.objects.get(pk=director.pk - 1)
    except Director.DoesNotExist:
        pass
    try:
        ctx["next"] = Director.objects.get(pk=director.pk + 1)
    except Director.DoesNotExist:
        pass
    return ctx


@register.inclusion_tag('movie_pagination.html')
def movie_pagination(movie: Movie):
    ctx = {

    }
    try:
        ctx["previous"] = Movie.objects.get(pk=movie.pk - 1)
    except Movie.DoesNotExist:
        pass
    try:
        ctx["next"] = Movie.objects.get(pk=movie.pk + 1)
    except Movie.DoesNotExist:
        pass
    return ctx


@register.simple_tag()
def no_movie_poster():
    return static('images/noposter.png')


@register.simple_tag()
def no_user_photo():
    return static('images/nophoto.png')
